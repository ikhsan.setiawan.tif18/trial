
package innerclasstest2;


/*
        Di buat oleh : Ikhsan s
        NIM          : 181511047
*/
public class InnerClassTest2 {

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        // TODO code application logic here
        OuterClass myOuter = new OuterClass();
        /*Constractor ke 1*/
        OuterClass.InnerClass myInner = myOuter.new InnerClass();
        /*Constractor ke 2*/
        System.out.println(myInner.MyinnerClass());
        
    }
    
}

 class OuterClass
    {
        int x =1;
        /*InnerClass*/
        class InnerClass
        {/*METHOD INNER CLASS*/
            public int MyinnerClass()
            {
                return x;
            }
        }
    }
    
