/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclass;

import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class InnerClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TalkingClock clock = new TalkingClock(1000, true);
        clock.start();
        
        // keep program running until user selects "Ok"         
        JOptionPane.showMessageDialog(null, "Quit program?");         
        System.exit(0);
    }
    
}

