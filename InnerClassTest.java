
package innerclasstest;

/*
        Di buat oleh : Ikhsan s
        NIM          : 181511047
*/
import java.awt.*; 
import java.awt.event.*; 
import java.util.*; 
import javax.swing.*; 
import javax.swing.Timer; 

public class InnerClassTest {


    public static void main(String[] args) {
        // TODO code application logic here
     TalkingClock clock = new TalkingClock(1000, true); 
     /**Constructor ke-1*/
      clock.start(); 
      
      /** Memulai fungsi clock*/
      // keep program running until user selects "Ok" 
       JOptionPane.showMessageDialog(null, "Quit program?"); 
         System.exit(0); 
    }    
}
 class TalkingClock 
 {
      
      private int interval;
      /** Variabel interval bertipe private integer */
      private boolean beep;
      /** Variabel beel bertipe private boolean */
      
      public TalkingClock(int interval, boolean beep) 
      { 
          this.interval = interval; 
          this.beep = beep; 
      }
      /*constractor TalkingClock*/
      
      public void start() 
      {      
          ActionListener listener = new TimePrinter();
          /*Constractor pada start */
          Timer t = new Timer(interval, listener); 
          t.start(); 
      
      }
     
      /*InnerClass*/
      public class TimePrinter implements ActionListener 
      {
            public void actionPerformed(ActionEvent event) 
            {
                System.out.println("At the tone, the time is " + new Date()); 
                if (beep) Toolkit.getDefaultToolkit().beep(); 
            } 
      }
 }
    
